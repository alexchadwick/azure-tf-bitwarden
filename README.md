# How

- Create `docker/secrets.env` file, using variables shown in reference `docker/secrets.env.example`
  - Create `identity/identity.pfx` for identity service. Use pfx password for `globalSettings__identityServer__certificatePassword` in `docker/secrets.env`
  - See pt. [4](https://bitwarden.com/help/article/install-on-premise-manual/)
- Set values for all .env files in `docker`
- Update webapp address in `web/app-id.json`
- Set `server_name` in `nginx/http.conf`
- Set Terraform environment variables and backend, using examples in `tf/env`
- Create the `docker-assets.zip` file
> zip -r docker-assets.zip . -x 'core/*' -x '.git/*' -x 'mssql/*' -x 'tf/*' -x 'ansible/*' -x 'logs/*'
- Deploy the Terraform resources

# Secrets

Application secrets are stored in `docker/secrets.env` file.
Note: `identity/identity.pfx` is password protected

# Summary
Deployed Azure resources:
- Ubuntu VM, configured as a Docker host using cloud-init userdata
- Azure Application Gateway, to load balance requests and terminate TLS
- Azure Key vault, to store the TLS certificate used by Application Gateway
- Managed Identity, authenticate Azure Key vault requests
- Azure Storage File Share, SMB3 share for container assets incl. secrets

Deploys Bitwarden selfhosted webapp, following the [docs](https://bitwarden.com/help/article/install-on-premise-manual/)

# TODO
- Replace MSSQL container with Azure SQL service
  - [docs](https://bitwarden.com/help/article/external-db/)
- Use Azure Container service?
  - HA: Multiple Nginx containers. Clustered db
- Container image version management: COREVERSION and WEBVERSION in `docker/.env`
  - [script includes recommended/latest (?) compatible core + web component versions](https://github.com/bitwarden/server/blob/4b346ff7eab2e58e9faf03fe78ba83c3eb695c11/scripts/bitwarden.sh#L37)
  - [No solution is provided for this](https://github.com/bitwarden/server/issues/307#issuecomment-473404439)
- Monitoring: Log analytics (network) and app insights (vm/container)
- x2 Pipelines
  - Deploy terraform managed Azure resources: Edit vm cloud-init userdata - Shouldn't deploy the containers as root
  - Deploy containers using SSH service connection

# Notes
- Azure only provides free TLS certificates for App Services
  - Options:
    - Import paid for cert into az vault and use app gateway
    - Deploy as app service

- Application Gateway:
  - Virtual Network:
    - Application Gateway requires it's own subnet
    - Public/Private Subnet for backend VMs
  - Add public IP to gateway
  - Assign Listeners (http/s) to ports, incl. tls cert
  - Add resources to backend pool
  - Create routing rules between listeners and backend pool

- Ruled out options:
  - Container Instances, can't load balance reliably
    - Application Gateway targets the private IP, rather than dynamically target the Container group
  - Azure Load Balancer, tcp layer 4 only. Can't terminate TLS
  - Creating the docker-asset files using Cloud-init write_files would have exposed the secrets in plaintext

- Use AWS instead?
  - ALB will dynamically target resources (containers, ec2 instances, etc). Unlike Azure Application Gateway which targets IPs which may change
    - Allows us to use Fargate
  - ACM TLS certificates are free
