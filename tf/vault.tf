resource "azurerm_user_assigned_identity" "agw" {
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
  name                = "${var.project_name}-agw1-msi"
}

data "azurerm_client_config" "current" {}

resource "azurerm_key_vault" "agw" {
  name                       = "${var.project_name}-kv1"
  location                   = azurerm_resource_group.this.location
  resource_group_name        = azurerm_resource_group.this.name
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days = 7
  purge_protection_enabled   = false
  sku_name                   = "standard"

  network_acls {
    default_action = "Allow"
    bypass         = "AzureServices"
  }
}

resource "azurerm_key_vault_access_policy" "builder" {
  key_vault_id = azurerm_key_vault.agw.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = data.azurerm_client_config.current.object_id

  certificate_permissions = [
    "create",
    "get",
    "recover",
    "list",
    "delete",
    "update",
    "purge"
  ]
}

resource "azurerm_key_vault_access_policy" "agw" {
  key_vault_id = azurerm_key_vault.agw.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = azurerm_user_assigned_identity.agw.principal_id

  certificate_permissions = [
    "get",
    "recover"
  ]

  secret_permissions = [
    "get"
  ]
}

resource "azurerm_key_vault_certificate" "this" {
  depends_on = [azurerm_key_vault_access_policy.builder]

  name         = var.certificate.name
  key_vault_id = azurerm_key_vault.agw.id

  certificate_policy {
    issuer_parameters {
      name = "Self"
    }

    key_properties {
      exportable = true
      key_size   = 2048
      key_type   = "RSA"
      reuse_key  = true
    }

    lifetime_action {
      action {
        action_type = "AutoRenew"
      }

      trigger {
        days_before_expiry = 30
      }
    }

    secret_properties {
      content_type = "application/x-pkcs12"
    }

    x509_certificate_properties {
      # Server Authentication = 1.3.6.1.5.5.7.3.1
      # Client Authentication = 1.3.6.1.5.5.7.3.2
      extended_key_usage = ["1.3.6.1.5.5.7.3.1"]

      key_usage = [
        "cRLSign",
        "dataEncipherment",
        "digitalSignature",
        "keyAgreement",
        "keyCertSign",
        "keyEncipherment",
      ]

      subject_alternative_names {
        dns_names = [var.certificate.domain]
      }

      subject            = "CN=${var.certificate.domain}"
      validity_in_months = 12
    }
  }
}

# Certificate is not available immediately after successful creation
#resource "time_sleep" "wait_60_seconds" {
#  depends_on = [azurerm_key_vault_certificate.mysite4]
#
#  create_duration = "60s"
#}