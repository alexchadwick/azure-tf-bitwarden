#cloud-config
users:
  - name: ${username}
    ssh-authorized-keys:
      - ${ssh_public_key}
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    groups: 
      - sudo
      - docker
    shell: /bin/bash

groups:
  - docker

apt:
  sources:
    docker.list:
      source: deb [arch=amd64] https://download.docker.com/linux/ubuntu $RELEASE stable
      keyid: 9DC858229FC7DD38854AE2D88D81803C0EBFCD88

package_upgrade: true
packages:
  - python3-pip
  - docker-ce
  - docker-ce-cli
  - containerd.io
  - zip

runcmd:
  # Need to upgrade pip
  - pip3 install -U pip
  - pip3 install docker-compose
  # Setup Docker assets
  - mkdir -p /opt/bitwarden
  - unzip ${docker_assets} -d /opt/bitwarden/
  # ONLY FOR TESTING: Docker compose actions
  - cd /opt/bitwarden/docker
  - docker-compose up -d