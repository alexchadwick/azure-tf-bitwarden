stage = "dev"
location = "uksouth"
project_name = "bitwarden"

certificate = {
    name = "mysite4"
    domain = "mysite4.com"
}

ssh = {
    username = "alex"
    public_key = "~/.ssh/id_rsa.pub"
    private_key = "~/.ssh/id_rsa"
    public_ip_prefix = "188.210.214.37/32"
}

#https://discourse.ubuntu.com/t/find-ubuntu-images-on-microsoft-azure/18918
vm = {
    size = "Standard_B2ms" #https://docs.microsoft.com/en-us/azure/virtual-machines/sizes-b-series-burstable
}

app_service_plan_config = {
    kind = "Linux",
    tier = "Basic",
    size = "B1"
}

application_gateway_sku = "Standard_v2"

application_gateway_capacity = {
    min = 1
    max = 2
}

#Availability Zones
zones    = ["1", "2", "3"]