variable "stage" {
  type = string
}
variable "location" {
  type    = string
  default = "uksouth"
}
variable "project_name" {
  type = string
}

variable "certificate" {
  type = map(any)
}

variable "ssh" {
  type = map(any)
}

variable "vm" {
  type = map(any)
}

variable "app_service_plan_config" {
  type = map(any)
  default = {
    kind = "Linux",
    tier = "Basic",
    size = "B1"
  }

  validation {
    condition     = contains(["FunctionApp", "Linux", "Windows", "elastic"], var.app_service_plan_config.kind)
    error_message = "Valid values for app_service_plan_config.kind are 'FunctionApp' (Consumption Plan), 'elastic' (Premium Consumption), 'Windows' or 'Linux' (Dedcated/Shared compute)."
  }
  validation {
    condition     = contains(["Dynamic", "Basic", "Standard", "PremiumV2", "PremiumV3", "Free", "Shared"], var.app_service_plan_config.tier)
    error_message = "Valid values for app_service_plan_config.tier are 'Dynamic' (Consumption Plan), 'Standard', 'Basic', 'PremiumV2', 'PremiumV3' (Dedcated compute) or 'Free', 'Shared' (Shared compute)."
  }
}

variable "application_gateway_sku" {
  type = string
}

variable "application_gateway_capacity" {
  type = map(any)
}

variable "zones" {
  type = list(any)
}

variable "http" {
  type = map(any)
  default = {
    "listener"      = "HttpListener-80"
    "frontend_port" = "fe-80"
    "route_rule"    = "route-http"
    "redirect_rule" = "redirect-http"
  }
}

variable "https" {
  type = map(any)
  default = {
    "listener"      = "HttpsListener-443"
    "frontend_port" = "fe-443"
    "route_rule"    = "route-https"
  }
}

variable "frontend_ip_config_public" {
  type    = string
  default = "publicFrontendIP"
}

variable "backend_pool" {
  type = map(any)
  default = {
    "name"          = "BackendPool"
    "http_settings" = "BackendHttpsSettings"
  }
}

variable "azure_file_share" {
  type = map(any)
  default = {
    name     = "azure-assets"
    zip_file = "up.zip"
  }
}